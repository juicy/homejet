Homejet::Application.routes.draw do

  root to: 'application#index'

  # Camera
  get 'camera' => 'camera#index', as: 'cams'
  get 'camera/:id' => 'camera#show', as: 'cam'
  get 'new_camera' => 'camera#new', as: 'new_cam'
  post 'cameras' => 'camera#create', as: 'cameras'
  put 'cameras/:id' => 'camera#update', as: 'camera'
  get 'edit_camera/:id' => 'camera#edit', as: 'edit_cam'
  get 'remove_camera/:id' => 'camera#destroy', as: 'remove_cam'

  # Sensor
  get 'sensors' => 'sensors#index', as: 'sensors'
  get 'sensors/:id' => 'sensors#show', as: 'sensor'
  get 'new_sensor' => 'sensors#new', as: 'new_sensor'
  post 'sensors' => 'sensors#create', as: 'sensors'
  put 'sensors/:id' => 'sensors#update', as: 'sensor'
  get 'edit_sensor/:id' => 'sensors#edit', as: 'edit_sensor'
  get 'remove_sensor/:id' => 'sensors#destroy', as: 'remove_sensor'

  # Outlet
  get 'outlets' => 'outlets#index', as: 'outlets'
  get 'outlets/:id' => 'outlets#show', as: 'outlet'
  get 'new_outlet' => 'outlets#new', as: 'new_outlet'
  post 'outlets' => 'outlets#create', as: 'outlets'
  put 'outlets/:id' => 'outlets#update', as: 'outlet'
  get 'edit_outlet/:id' => 'outlets#edit', as: 'edit_outlet'
  get 'remove_outlet/:id' => 'outlets#destroy', as: 'remove_outlet'

  # Script
  get 'scripts' => 'scripts#index', as: 'scripts'
  get 'scripts/:id' => 'scripts#show', as: 'script'
  get 'new_script' => 'scripts#new', as: 'new_script'
  post 'scripts' => 'scripts#create', as: 'scripts'
  put 'scripts/:id' => 'scripts#update', as: 'script'
  get 'edit_script/:id' => 'scripts#edit', as: 'edit_script'
  get 'remove_script/:id' => 'scripts#destroy', as: 'remove_script'

  # Users
  get 'profile' => 'users#index', as: 'user'
  put 'profile' => 'users#update', as: 'user_put'
  get 'profile/phone' => 'users#phone', as: 'user_phone'
  put 'profile/phone' => 'users#phone_put', as: 'user_phone_put'
  put 'profile/linking' => 'users#linking_put', as: 'user_linking_put'
  get 'profile/unlinking' => 'users#unlinking', as: 'user_unlinking'
  get 'profile/secure' => 'users#secure', as: 'secure'
  get 'sms_authorization' => 'users#sms_authorization', as: 'sms_authorization'
  get 'register' => 'users#new', as: 'register'
  post 'register' => 'users#create', as: 'users'
  get 'logout' => 'users#logout', as: 'logout'
  get 'login' => 'users#login', as: 'login'
  post 'login' => 'users#create_session', as: 'create_session'
  post 'login_sms' => 'users#create_session_sms', as: 'create_session_sms'

  # Sender 
  post 'sender' => 'sender#index', as: 'sender'

end
