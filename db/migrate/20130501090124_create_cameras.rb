class CreateCameras < ActiveRecord::Migration
  def change
    create_table :cameras do |t|
      t.string :ip
      t.string :name
      t.string :title

      t.timestamps
    end
  end
end
