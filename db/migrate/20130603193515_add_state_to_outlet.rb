class AddStateToOutlet < ActiveRecord::Migration
  def change
    add_column :outlets, :state, :boolean
  end
end