class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.string :name
      t.integer :device # 0 cam, 1 sensor, 2 outlet

      t.timestamps
    end
  end
end
