class CreateScripts < ActiveRecord::Migration
  def change
    create_table :scripts do |t|
      t.integer :if_camera_id
      t.integer :if_outlet_id
      t.integer :if_sensor_id
      t.integer :then_camera_id
      t.integer :then_outlet_id
      t.integer :then_sensor_id
      t.integer :if_action_id
      t.integer :then_action_id
      t.boolean :sms
      t.boolean :email
      t.boolean :pause
      t.string :name

      t.timestamps
    end
  end
end
