class AddFieldsToUser2 < ActiveRecord::Migration
  def change
    add_column :users, :sms_authorization, :boolean
    add_column :users, :sms_authorization_code, :string
  end
end