class ChangeDevType < ActiveRecord::Migration
  def up
    change_column :actions, :device, :string
  end

  def down
  end
end