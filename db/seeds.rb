#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Action.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('actions')
Action.create(:id => 1, :name => 'Фиксацию движения', :text => 'фиксирует движение', device: 'camera', :when => 'if')
Action.create(:id => 2, :name => 'Фиксацию t ниже 20°', :text => 'фиксирует  t ниже 20°', device: 'sensor', :when => 'if')
Action.create(:id => 3, :name => 'Запись', :text => 'начинает записывает', device: 'camera', :when => 'then')
Action.create(:id => 4, :name => 'Остановку записи', :text => 'прекращает записывать', device: 'camera', :when => 'then')
Action.create(:id => 5, :name => 'Включение', :text => 'включается', device: 'outlet', :when => 'then')
Action.create(:id => 6, :name => 'Отключение', :text => 'выключается', device: 'outlet', :when => 'then')
