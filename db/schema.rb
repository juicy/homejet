# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130609151152) do

  create_table "actions", :force => true do |t|
    t.string   "name"
    t.string   "device"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "when"
    t.string   "text"
  end

  create_table "cameras", :force => true do |t|
    t.string   "ip"
    t.string   "name"
    t.string   "title"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
  end

  create_table "outlets", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
    t.boolean  "state"
  end

  create_table "scripts", :force => true do |t|
    t.integer  "if_camera_id"
    t.integer  "if_outlet_id"
    t.integer  "if_sensor_id"
    t.integer  "then_camera_id"
    t.integer  "then_outlet_id"
    t.integer  "then_sensor_id"
    t.integer  "if_action_id"
    t.integer  "then_action_id"
    t.boolean  "sms"
    t.boolean  "email"
    t.boolean  "pause"
    t.string   "name"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "user_id"
  end

  create_table "sensors", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "surname"
    t.string   "email"
    t.string   "password"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "phone"
    t.boolean  "phone_validate"
    t.string   "linking_code"
    t.boolean  "sms_authorization"
    t.string   "sms_authorization_code"
  end

end
