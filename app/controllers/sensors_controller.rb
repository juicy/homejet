class SensorsController < ApplicationController

  before_filter :login?
  before_filter :find_sensor

  def show
    @sidebar = 4
  end

  def index
    @sidebar = 4
    @sensors = @user.sensors
  end

  def new
    @sidebar = 4
    @sensor = Sensor.new
  end

  def edit
    @sidebar = 4
  end

  def update
    @sidebar = 4
    @sensor.user = @user
    if @sensor.update_attributes(params[:sensor])
      redirect_to :action => "index"
    end
  end

  def create
    @sidebar = 4
    @sensor = Sensor.new(params[:sensor])
    @sensor.user = @user
    if @sensor.save
      redirect_to :action => "index"
    end
  end

  def destroy
    @sensor.destroy
    redirect_to :action => "index"
  end


private

  def find_sensor
    @sensor = Sensor.find(params[:id]) if params[:id]
  end

  def login?
    unless session[:user]
      redirect_to login_url
    end
  end

end
