class OutletsController < ApplicationController

  before_filter :login?
  before_filter :find_outlet

  def show
    @sidebar = 5
  end

  def index
    @sidebar = 5
    @outlets = @user.outlets
  end

  def new
    @sidebar = 5
    @outlet = Outlet.new
  end

  def edit
    @sidebar = 5
  end

  def update
    @sidebar = 5
    @outlet.user = @user
    if @outlet.update_attributes(params[:outlet])
      redirect_to :action => "index"
    end
  end

  def create
    @sidebar = 5
    @outlet = Outlet.new(params[:outlet])
    @outlet.user = @user
    if @outlet.save
      redirect_to :action => "index"
    end
  end

  def destroy
    @outlet.destroy
    redirect_to :action => "index"
  end


private

  def find_outlet
    @outlet = Outlet.find(params[:id]) if params[:id]
  end

  def login?
    unless session[:user]
      redirect_to login_url
    end
  end

end