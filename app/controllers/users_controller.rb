#encoding: utf-8
class UsersController < ApplicationController

  require 'net/http'
  require 'uri'

  before_filter :login?, :except => [:new, :create, :login, :sms_authorization, :create_session, :create_session_sms]
  before_filter :user_find, :except => [:new, :create, :login, :sms_authorization, :create_session, :create_session_sms]

  def update

    @err = true

    if @user.update_attributes(params[:user])
      @err = false
    end

    respond_to do |format|
      format.js
    end

  end

  def index
    @sidebar = 2
  end

  def secure
    @sidebar = 2
  end

  def phone
    @sidebar = 2
  end

  def unlinking
    @user.phone = ''
    @user.phone_validate = false
    @user.sms_authorization = false
    @user.save
    redirect_to user_phone_url
  end

  def linking_put

    if params[:linking_code] == @user.linking_code
      @user.linking_code = ''
      @user.phone_validate = true
      @user.save
    end

    respond_to do |format|
      format.js
    end

  end

  def phone_put

    @err = true

    if @user.update_attributes(params[:user])
      @user.linking_code = (0...5).map{ (0..9).to_a[rand(10)] }.join
      Net::HTTP.post_form URI.parse('http://api.smsfeedback.ru/messages/v2/send'), { :login => 'homejet', :password => 'Ynsgwmue5979HO', :phone => @user.phone, :text => "Код: #{@user.linking_code}", :sender => 'Homejet' }
      
      @user.save
      @err = false
    end

    respond_to do |format|
      format.js
    end
  end

  def new
    @user = User.new
    render 'new', layout: 'login'
  end

  def create
    @user = User.new(params[:user])
    @user.password = Digest::MD5.hexdigest(@user.password)
    @user.password_confirmation = Digest::MD5.hexdigest(@user.password_confirmation)
    if @user.save
      session[:user] = @user.id
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def login
    render 'login', layout: 'login'
  end

  def sms_authorization
    render 'sms_authorization', layout: 'login'
  end

  def create_session_sms
    @user = User.find(session[:sms_authorization_user])
    if params[:sms_authorization_code] == @user.sms_authorization_code
      session[:user] = @user.id
      session[:sms_authorization] = false
    end
    respond_to do |format|
      format.js
    end
  end

  def create_session
    @user = User.find_by_email(params[:email])
    if @user && @user.password == Digest::MD5.hexdigest(params[:password])
      unless @user.sms_authorization
        session[:user] = @user.id
      else
        session[:sms_authorization_user] = @user.id
        session[:sms_authorization] = true
        @user.sms_authorization_code = (0...5).map{ (0..9).to_a[rand(10)] }.join
        if !@user.phone.blank? && @user.phone_validate
          Net::HTTP.post_form URI.parse('http://api.smsfeedback.ru/messages/v2/send'), { :login => 'homejet', :password => 'Ynsgwmue5979HO', :phone => @user.phone, :text => "Код: #{@user.sms_authorization_code}", :sender => 'Homejet' }
        end
        @user.save
      end
    end
    respond_to do |format|
      format.js
    end
  end

  def logout
    session[:user] = nil
    redirect_to root_url
  end

end
