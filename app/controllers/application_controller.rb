class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :login?, :only => [:index]
  before_filter :user_find

  def index
    @cams = @user.cams
    @sensors = @user.sensors
    @outlets = @user.outlets
    @scripts = @user.scripts
  end

  def login?
    unless session[:user]
      redirect_to login_url
    end
  end

  def user_find
    @user = User.find(session[:user])
  end

end
