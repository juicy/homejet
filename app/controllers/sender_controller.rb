class SenderController < ApplicationController

  before_filter :user_find

  def index

    rendered = false

    if params[:device] == 'outlet'

      if params[:do] == 'toggle_state'
        outlet = Outlet.find(params[:id])
        outlet.toggle_state
        rendered = true
        render :text => "outlet #{params[:id]} state toggled"
      end

    end

    render :text => nil if !rendered

  end

  def user_find
    @user = User.find(session[:user])
  end

end
