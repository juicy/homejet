class ScriptsController < ApplicationController
  before_filter :login?
  before_filter :find_script

  def show
    @sidebar = 6
  end

  def index
    @sidebar = 6
    @scripts = @user.scripts
  end

  def new
    @sidebar = 6
    @script = Script.new
  end

  def edit
    @sidebar = 6
  end

  def update
    @sidebar = 6

    @script.user = @user
    @script.if_camera_id = nil
    @script.if_outlet_id = nil
    @script.if_sensor_id = nil
    case params[:if_device_type]
    when 'camera'
      @script.if_camera_id = params[:if_id]
    when 'outlet'
      @script.if_outlet_id = params[:if_id]
    when 'sensor'
      @script.if_sensor_id = params[:if_id]
    end

    @script.then_camera_id = nil
    @script.then_outlet_id = nil
    @script.then_sensor_id = nil
    case params[:then_device_type]
    when 'camera'
      @script.then_camera_id = params[:then_id]
    when 'outlet'
      @script.then_outlet_id = params[:then_id]
    when 'sensor'
      @script.then_sensor_id = params[:then_id]
    end

    if @script.update_attributes(params[:script])
      redirect_to :action => "index"
    end
  end

  def create
    @sidebar = 6
    @script = Script.new(params[:script])
    @script.user = @user
    case params[:if_device_type]
    when 'camera'
      @script.if_camera_id = params[:if_id]
    when 'outlet'
      @script.if_outlet_id = params[:if_id]
    when 'sensor'
      @script.if_sensor_id = params[:if_id]
    end
    
    case params[:then_device_type]
    when 'camera'
      @script.then_camera_id = params[:then_id]
    when 'outlet'
      @script.then_outlet_id = params[:then_id]
    when 'sensor'
      @script.then_sensor_id = params[:then_id]
    end

    if @script.save
      redirect_to :action => "index"
    end
  end

  def destroy
    @script.destroy
    redirect_to :action => "index"
  end

private

  def find_script
    @script = Script.find(params[:id]) if params[:id]
  end

  def login?
    unless session[:user]
      redirect_to login_url
    end
  end


end
