class CameraController < ApplicationController
  before_filter :login?
  before_filter :find_camera

  def show
    @sidebar = 3
  end

  def index
    @sidebar = 3
    @cams = @user.cams
  end

  def new
    @sidebar = 3
    @camera = Camera.new
  end

  def edit
    @sidebar = 3
  end

  def update
    @sidebar = 3
    @camera.user = @user
    if @camera.update_attributes(params[:camera])
      redirect_to :action => "index"
    end
  end

  def create
    @sidebar = 3
    @camera = Camera.new(params[:camera])
    @camera.user = @user
    if @camera.save
      redirect_to :action => "index"
    end
  end

  def destroy
    @camera.destroy
    redirect_to :action => "index"
  end


private

  def find_camera
    @camera = Camera.find(params[:id]) if params[:id]
  end

  def login?
    unless session[:user]
      redirect_to login_url
    end
  end

end