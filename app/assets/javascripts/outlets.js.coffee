# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready(() ->
  $('.c_flip').click(() ->
    toggle_c_clip(this);
  );
)



toggle_c_clip = (block) ->
  time = 300
  if $(block).hasClass('on')
    $(block).find('.bg').animate({'left': 0}, time);
    $(block).find('.handle').animate({'left': 36}, time);
    $(block).removeClass('on')
  else
    $(block).find('.bg').animate({'left': -50}, time);
    $(block).find('.handle').animate({'left': -11}, time);
    $(block).addClass('on')
    