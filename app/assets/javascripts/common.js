//common js behavior

$(function(){
  initFlip();
  paintTable();
  $('.b-camera__body a').click(function() { return false; });
  $('.index_carusel').each(function() {
    if ($(this).hasClass('big')) {
      carusel (this, 2, 384, 'a.l', 'a.r', '.w', 9999999);
    } else {
      carusel (this, 3, 258, 'a.l', 'a.r', '.w', 9999999);
    }
  });
});

function initFlip(){
  $('.b-flip__item').click(function(){
      $(this).toggleClass('b-flip__item_active');
  });

}
function paintTable(){
  $('.archive-journal, .archive').each(function(){
    $('.archive-item:odd',this).addClass('odd');
    $('.archive-item:even',this).addClass('even');
  });
}

function carusel (block, in_window, width, left, right, wrap, time) {
  var th = 0;
  var max = $(block).find('.mini').length - in_window;
  var hover = false;

  setTimeout(function() {
    auto();
  }, time);

  $(block).hover(function() {
    hover = true;
  }, function() {
    hover = false;
  });

  $(block).find(left).click(function() {
    to(th - 1);
    return false;
  });
  $(block).find(right).click(function() {
    to(th + 1);
    return false;
  });

  function auto() {
    if (!hover) {
      to(th + 1);
    }
    setTimeout(function() {
      auto();
    }, time);
  }

  function to (num) {
    if (num < 0)   { num = max };
    if (num > max) { num = 0 };
    $(block).find(wrap).animate({
      'margin-left': num * -1 * width
    }, 500, function() {
      th = num;
    })
  }
}

function send (d) {
  $.ajax({
    url: '/sender',
    type: 'POST',
    data: d,
    beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
    complete: function(xhr, textStatus) {
      //called when complete
    },
    success: function(data, textStatus, xhr) {
      //called when successful
    },
    error: function(xhr, textStatus, errorThrown) {
      //called when there is an error
    }
  });
  return false;
}