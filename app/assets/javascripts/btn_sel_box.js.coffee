$(document).ready(() ->
  btn_sel_box('.btn_sel_box')
)

btn_sel_box = (boxs) ->
  $(boxs).each(() ->
    box = $(this)
    options = $(this).find('.option')
    label_content = $(this).find('.label_content')
    input_type = $(this).find('input.type[type="hidden"]')
    input_id = $(this).find('input.id[type="hidden"]')
    
    $('body').click(() ->
      if $(box).hasClass('open')
        $(box).removeClass('open')
    )
    $(box).find('.btn_menu').click((event) ->
      event.stopPropagation();
      $(box).addClass('open')
    )
    $(options).click(() ->
      $(box).removeClass('open')
      $(label_content).html($(this).clone())
      $(input_type).val($(this).attr('rel'))
      $(input_id).val($(this).attr('value'))
      if $(box).hasClass('ch')
        $(box).parent().find('.btn_sel_box:eq(1) .option').css('display', 'none')
        act = $(box).parent().find('.btn_sel_box:eq(1) .content .option[rel="'+$(this).attr('rel')+'"]')
        $(act).css('display', 'block')
        $(act).first().click()
      else
    )
    if $(input_id).val() != '' || $(input_type).val() != ''
      if $(input_id).val() && $(input_type).val()
        $(this).find('.content .option[rel="'+$(input_type).val()+'"][value="'+$(input_id).val()+'"]').click()
      else
        $(this).find('.content .option[value="'+$(input_id).val()+'"]').click()
  )