class User < ActiveRecord::Base

  attr_accessible :email, :name, :password, :password_confirmation, :surname, :phone, :linking_code, :phone_validate,
                  :sms_authorization, :sms_authorization_code

  has_many :cams, :class_name => "Camera", :foreign_key => "user_id"
  has_many :sensors, :class_name => "Sensor", :foreign_key => "user_id"
  has_many :outlets, :class_name => "Outlet", :foreign_key => "user_id"
  has_many :scripts, :class_name => "Script", :foreign_key => "user_id"

  validates :name, :email, :surname, :password, presence: true
  validates :email, :uniqueness => true
  validates :password, confirmation: true

  def full_name
    "#{self.name} #{self.surname}"
  end

end