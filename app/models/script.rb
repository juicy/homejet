class Script < ActiveRecord::Base
  attr_accessible :email, :if_action_id, :if_camera_id, :if_outlet_id, :if_sensor_id, :name, :pause, :sms, :then_action_id, :then_camera_id, :then_outlet_id, :then_sensor_id

  has_one :if_camera, :class_name => "Camera", :foreign_key => "if_camera_id"
  has_one :if_outlet, :class_name => "Outlet", :foreign_key => "if_outlet_id"
  has_one :if_sensor, :class_name => "Sensor", :foreign_key => "if_sensor_id"

  has_one :then_camera, :class_name => "Camera", :foreign_key => "then_camera_id"
  has_one :then_outlet, :class_name => "Outlet", :foreign_key => "then_outlet_id"
  has_one :then_sensor, :class_name => "Sensor", :foreign_key => "then_sensor_id"

  has_one :if_action, :class_name => "Action", :foreign_key => "if_action_id"
  has_one :then_action, :class_name => "Action", :foreign_key => "then_action_id"

  belongs_to :user, :class_name => "User", :foreign_key => "user_id"

end
