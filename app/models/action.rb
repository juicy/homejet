class Action < ActiveRecord::Base
  attr_accessible :device, :name, :when, :text

  belongs_to :script_if, :class_name => "Script", :foreign_key => "if_action_id"
  belongs_to :script_then, :class_name => "Script", :foreign_key => "then_action_id"

  scope :if, where(:when => 'if')
  scope :then, where(:when => 'then')

end
