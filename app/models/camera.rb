class Camera < ActiveRecord::Base
  attr_accessible :ip, :name, :title, :user_id

  belongs_to :user, :class_name => "User", :foreign_key => "user_id"

  belongs_to :script_if, :class_name => "Script", :foreign_key => "if_camera_id"
  belongs_to :script_then, :class_name => "Script", :foreign_key => "then_camera_id"


end
