class Sensor < ActiveRecord::Base
  attr_accessible :name

  belongs_to :user, :class_name => "User", :foreign_key => "user_id"

  belongs_to :script_if, :class_name => "Script", :foreign_key => "if_sensor_id"
  belongs_to :script_then, :class_name => "Script", :foreign_key => "then_sensor_id"

end
